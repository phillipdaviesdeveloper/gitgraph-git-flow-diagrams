#GitGraph - Example Implementation

Allows users to generate Git Branching models on the fly. 

Add new graphs or alter existing graph file in ```assets / js / modules / graph /``` see example 'project-flow.js'

Custom templates can be added in ```assets / js / modules / graph / templates /``` see example 'project template'.

Graphs added by require in ```assets / js /app.js``` 


###Install

To view: 

* Fork or download the repo
* Setup your local server or upload to a host
* Navigate to the folder in your browser

To edit:

* Fork or download the repo
* CD to install directory
* run ```npm install``` to load dependencies
* run ```gulp watch``` to compile any changes
* Either change the example files or create your own  

###Documentation 

Find GitGraph docs at http://gitgraphjs.com/


###Requirements

* Local or hosted server running a LAMP stack (MAMP / WAMP should work fine)
* NPM https://www.npmjs.com/
* Gulp http://gulpjs.com/


###Example 

![1116685145-download.png](https://bitbucket.org/repo/gaxMpn/images/2591881339-1116685145-download.png)